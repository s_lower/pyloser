

#=========================================================
# READ IN PARTICLES AND LOAD REQUIRED STAR AND GAS ARRAYS
#=========================================================

import caesar
from readgadget import *
import numpy as np
from auxloser import t_elapsed,get_tage,init_cosmo,get_lumtoflux,hubble_z
from astropy import constants as const
import time
import psutil
import os


# used for making 'image': fills a list of pixels with lists of gas or stars within each pixel
def fillpix(pos,accept,Npix,center,size,idir0):
    idir1 = (idir0+1)%3
    idir2 = (idir0+2)%3
    px = pos[:,idir1]
    py = pos[:,idir2]
    pz = pos[:,idir0]
    xlim = [(center[idir1]-0.5*size[idir1]),(center[idir1]+0.5*size[idir1])]
    ylim = [(center[idir2]-0.5*size[idir2]),(center[idir2]+0.5*size[idir2])]
    if size[idir0]<0: zlim = [-1.e20,1.e20]  # negative value for size means project the entire box
    else: zlim = [(center[idir0]-0.5*size[idir0]),(center[idir0]+0.5*size[idir0])]
    if size[idir1]<=0 or size[idir2]<=0:
        print("I'm a loser baby: Can't have a negative size parameter for a transverse direction",size)
    in_image = (px>=xlim[0]) & (px<xlim[1]) & (py>=ylim[0]) & (py<ylim[1]) & (pz>=zlim[0]) & (pz<zlim[1])
    for i in range(len(in_image)):
        if in_image[i]: accept[i] = i
    #print len(in_image),len(accept),len(accept>=0),accept
    pix = [None]*(Npix[0]*Npix[1])
    ix = Npix[0]*(px[accept>=0]-xlim[0])/size[idir1]
    iy = Npix[1]*(py[accept>=0]-ylim[0])/size[idir2]
    index = np.where(accept>=0)[0]
    for i in range(Npix[0]):
        for j in range(Npix[1]):
            ind = np.where((ix >= i) & (ix < i+1) & (iy >= j) & (iy < j+1))[0]
            if len(ind) > 0: pix[i + Npix[0]*j] = list(index[ind])
    return pix,accept

# Routine to load only required particles for each desired object.  Puts it into a list of Nobj numpy arrays
def load_quant(qname,snapfile,ptype,accept,map_type,Nobjs=None,objs=None,pixstars=None,pixgas=None,elem=None):
    quant = readsnap(snapfile,qname,ptype,units=1,suppress=1) 
    if elem != None:
        quant = quant[:,elem]
    myquant = [None]*Nobjs
    for i in range(Nobjs):        # get the required info for star particles in halos.  Gives a list of lists, length=Nobjs
        if map_type == 'object' and ptype == 'star': plist = np.array(objs[i].slist)  # plist contains particle indices of stars in object i
        if map_type == 'object' and ptype == 'gas': plist = np.array(objs[i].glist)  # plist contains particle indices of stars in object i
        if map_type == 'image' and ptype == 'star': plist = np.array(pixstars[i])
        if map_type == 'image' and ptype == 'gas': plist = np.array(pixgas[i])
        myquant[i] = np.asarray([quant[p] for p in plist if accept[p]>=0])
        #myquant[i] = quant[np.in1d(accept,plist)]
    return np.asarray(myquant)

# Read in particle properties, set up objects to process
def load_sim(snapfile,sim,objs,params,nproc,TINIT):
# initialize simulation parameters
    pid = os.getpid()
    py = psutil.Process(pid)
    print('pyloser : reading snapshot %s, z=%g, ngas=%d, nstar=%d, RAM=%g GB [t=%g s]'%(snapfile,np.round(sim.simulation.redshift,3),sim.simulation.ngas,sim.simulation.nstar,py.memory_info()[0]/2.**30,t_elapsed(TINIT)))
    # set up cosmology
    h = sim.simulation.hubble_constant
    cosmo = init_cosmo(sim)
    lumtoflux = get_lumtoflux(cosmo,sim.simulation.redshift)
    print('pyloser : Cosmology is %s, H_z=%g km/s/Mpc  [t=%g s]'%(cosmo,hubble_z(sim),t_elapsed(TINIT)))

# determine which gas+star particles are required (gaccept,saccept) based on map_type
    saccept = np.zeros(readheader(snapfile,'starcount'),dtype=int)-1  # -1 means not accepted
    gaccept = np.zeros(readheader(snapfile,'gascount'),dtype=int)-1
    if params['map_type'] == 'object':
        Nobjs = len(objs)
        iobjs = [i.GroupID for i in objs]
        print('pyloser : Processing %d galaxies/halos, RAM=%g [t=%g s]'%(Nobjs,py.memory_info()[0]/2.**30,t_elapsed(TINIT)))
        for i in range(Nobjs):        # figure out which gas+star particles are in galaxies, set their accept>0
            for j in objs[i].slist: saccept[j] = j
            for j in objs[i].glist: gaccept[j] = j
        #print 1.*len(saccept[saccept])/len(saccept),1.*len(gaccept[gaccept])/len(gaccept)
        pixstars = None
        pixgas = None
    elif params['map_type'] == 'image':
        Npix = eval(params['image_Npix'])
        Nobjs = Npix[0]*Npix[1]
        try: center = eval(params['image_center']).value
        except: center = eval(params['image_center'])
        try: size = eval(params['image_size']).value
        except: size = eval(params['image_size'])
        print('pyloser : Making %d x %d image with c=%s size=%s RAM=%g [t=%g s]'%(Npix[0],Npix[1],center,size,py.memory_info()[0]/2.**30,t_elapsed(TINIT)))
        # compile list of gas and star particles within each pixel
        spall = readsnap(snapfile,'pos','star',units=1,suppress=1)/h  # comov kpc
        pixstars,saccept = fillpix(spall,saccept,Npix,center,size,int(params['view_dir']))
        gpall = readsnap(snapfile,'pos','gas',units=1,suppress=1)/h  # comov kpc
        pixgas,gaccept = fillpix(gpall,gaccept,Npix,center,size,int(params['view_dir']))
        pixnum = [None]*Nobjs
        if params['output_type'] == 'phot' or params['output_type'] == 'spec' or 'star' in params['output_type']:
            for i in range(Nobjs): 
                if pixstars[i] != None: 
                    pixnum[i] = i        # set up index of pixels
                    if pixgas[i] == None: pixgas[i] =[] # make empty arrays for pixels with stars but no gas
                    #if len(pixstars[i])>100: print(i,len(pixstars[i]),len(pixgas[i]),pixgas[i])
                else: pixgas[i] = None  # discard all gas in pixels with no stars
        elif params['output_type'] == 'gas' or params['output_type'] == 'datacube' or params['output_type'] == 'phys':
            for i in range(Nobjs): 
                if pixstars[i] != None or pixgas[i] != None: 
                    pixnum[i] = i        # set up index of pixels
                    if pixstars[i] == None: pixstars[i] =[] # make empty arrays for pixels with gas but no stars
                    if pixgas[i] == None: pixgas[i] =[] # make empty arrays for pixels with stars but no gas
                    #if len(pixstars[i])>100: print(i,len(pixstars[i]),len(pixgas[i]),pixgas[i])
        else: print('output type %s not found'%(params['output_type']))
        # strip out all pixels that don't have any stars or gas
        iobjs = np.asarray([x for x in pixnum if x is not None])
        if params['output_type'] == 'phot' or params['output_type'] == 'spec' or 'star' in params['output_type']: # for photometry, only need pixels with stars
            pixgas = np.asarray([x for x in pixgas if x is not None])
            pixstars = np.asarray([x for x in pixstars if x is not None])
        elif params['output_type'] == 'gas' or params['output_type'] == 'datacube' or params['output_type'] == 'phys':
            pixgas = np.asarray([pixgas[i] for i in range(len(pixnum)) if pixnum[i] is not None])
            pixstars = np.asarray([pixstars[i] for i in range(len(pixnum)) if pixnum[i] is not None])
        assert (len(iobjs) == len(pixgas)) & (len(iobjs) == len(pixstars))
        Nobjs = len(iobjs)        # these pixels are now our objects for which we compute things
    else:
        print("I'm a loser baby: map type",params['map_type'],"not found")

# set up metallicity indices
    star_metal_index = eval(params['star_metal_index'])    # metallicity index to use for SSP
    gas_metal_index = eval(params['gas_metal_index'])    # metallicity index to use for LOS absorption (e.g. for Gizmo-Mufasa: 0=total, 4=O, 10=Fe)

# read the star particle info
    print('pyloser : Reading star particle info for %d stars. RAM=%g [t= %g s]'%(readheader(snapfile,'starcount'),py.memory_info()[0]/2.**30,t_elapsed(TINIT)))  # memory usage in GB
    smass = load_quant('mass',snapfile,'star',saccept,params['map_type'],objs=objs,Nobjs=Nobjs,pixstars=pixstars)/h  # in Mo
    spos = load_quant('pos',snapfile,'star',saccept,params['map_type'],objs=objs,Nobjs=Nobjs,pixstars=pixstars)/h  # in ckpc
    svlos = load_quant('vel',snapfile,'star',saccept,params['map_type'],objs=objs,Nobjs=Nobjs,elem=int(params['view_dir']),pixstars=pixstars)  # in km/s
    smet = load_quant('Metallicity',snapfile,'star',saccept,params['map_type'],objs=objs,Nobjs=Nobjs,elem=star_metal_index,pixstars=pixstars)  # in metal mass fraction
    #print(len(smet),smet)
    sage = load_quant('age',snapfile,'star',saccept,params['map_type'],objs=objs,Nobjs=Nobjs,pixstars=pixstars)  # expansion factor at time of formation
    for i in range(Nobjs): sage[i] = get_tage(sage[i])  # convert to age of star in Gyr 

# set up gas particle acceptance criteria based on input gas_crit
    if 'gt' in params['gas_crit']: gt = readsnap(snapfile,'u','gas',units=1,suppress=1) # K
    if 'gnh' in params['gas_crit']: gnh = readsnap(snapfile,'rho','gas',units=1,suppress=1)*h**2*(0.75/const.m_p.to('g').value) # to physical H atoms/cm^3
    if 'gdel' in params['gas_crit']: gdel = readsnap(snapfile,'delaytime','gas',units=1,suppress=1) # if >0 it's currently a wind
    if 'gnw' in params['gas_crit']: gnw = readsnap(snapfile,'NWindLaunches','gas',suppress=1) # number of launches; AGN launches are stored in 1000's place
    if 'gsf' in params['gas_crit']: gsf = readsnap(snapfile,'sfr','gas',suppress=1) # SFR Mo/yr
    if 'gmet' in params['gas_crit']: gmet = readsnap(snapfile,'Metallicity','gas',units=1,suppress=1,elem=gas_metal_index) # metallicity
    if 'gd' in params['gas_crit']: gd = readsnap(snapfile,'Dust_Masses','gas',suppress=1)*1.e10/h  # dust mass in Mo
    gas_crit = eval(params['gas_crit'])        # set up gas particle acceptance criteria
    gaccept = np.array([i if (gaccept[i]>0)&(gas_crit[i]) else -1 for i in range(len(gaccept))])

# load in gas particle info 
    print('pyloser : Reading gas particle info for %d gas. RAM=%g [t= %g s]'%(len(gaccept),py.memory_info()[0]/2.**30,t_elapsed(TINIT)))  # memory usage in GB
    gmass = load_quant('mass',snapfile,'gas',gaccept,params['map_type'],objs=objs,Nobjs=Nobjs,pixgas=pixgas)/h
    gpos = load_quant('pos',snapfile,'gas',gaccept,params['map_type'],objs=objs,Nobjs=Nobjs,pixgas=pixgas)/h
    if eval(params['gas_metal_index']) >= 0:  # use specified metallicity to compute dust extinction
        gmet = load_quant('Metallicity',snapfile,'gas',gaccept,params['map_type'],objs=objs,Nobjs=Nobjs,elem=gas_metal_index,pixgas=pixgas)
    elif eval(params['gas_metal_index']) == -1:  # directly use the dust mass to compute dust extinction
        gmet = load_quant('Dust_Masses',snapfile,'gas',gaccept,params['map_type'],objs=objs,Nobjs=Nobjs,pixgas=pixgas)*1.e10/h
        gmet /= gmass  # gZtot (and eventually gmet) in this case holds the dust mass fraction, not the gas-phase metal mass fraction
    else:
        sys.exit("I'm a loser baby: gas_metal_index %d unrecognized"%params['gas_metal_index'])
    gsfr = load_quant('sfr',snapfile,'gas',gaccept,params['map_type'],objs=objs,Nobjs=Nobjs,pixgas=pixgas)
    ghsm = load_quant('SmoothingLength',snapfile,'gas',gaccept,params['map_type'],objs=objs,Nobjs=Nobjs,pixgas=pixgas)/h
    if params['output_type'] == 'gas' or params['output_type'] == 'datacube' or params['output_type'] == 'phys':
        gvlos = load_quant('vel',snapfile,'gas',gaccept,params['map_type'],objs=objs,Nobjs=Nobjs,elem=int(params['view_dir']), pixgas=pixgas)
        gneut = load_quant('nh',snapfile,'gas',gaccept,params['map_type'],objs=objs,Nobjs=Nobjs,pixgas=pixgas)
        gmol = load_quant('fH2',snapfile,'gas',gaccept,params['map_type'],objs=objs,Nobjs=Nobjs,pixgas=pixgas)
        gnh = load_quant('rho',snapfile,'gas',gaccept,params['map_type'],objs=objs,Nobjs=Nobjs,pixgas=pixgas)*h**2*(0.75/const.m_p.to('g').value)
        gnelec = load_quant('ne',snapfile,'gas',gaccept,params['map_type'],objs=objs,Nobjs=Nobjs,pixgas=pixgas)
        gnelec = gnelec*gnh  # electron density in units of H atoms cm^-3
        gtemp = load_quant('u',snapfile,'gas',gaccept,params['map_type'],objs=objs,Nobjs=Nobjs,pixgas=pixgas)

    #print len(gaccept),len(gaccept[gaccept>=0]),len(gm),len(saccept),len(saccept[saccept>=0]),len(sm),py.memory_info()[0]/2.**30
    '''
    # Fill star and gas arrays 
    print('pyloser : Filling accepted particles arrays: nstar= %d ngas= %d. RAM=%g [t= %g s]'%(len(saccept[saccept>=0]),len(gaccept[gaccept>=0]),py.memory_info()[0]/2.**30,t_elapsed(TINIT)))  # memory usage in GB
    smass = [None]*Nobjs
    spos = [None]*Nobjs
    svlos = [None]*Nobjs
    sage = [None]*Nobjs
    smet = [None]*Nobjs
    for i in range(Nobjs):        # get the required info for star particles in halos.  Gives a list of lists, length=Nobjs
        if params['map_type'] == 'object': plist = np.array(objs[i].slist)  # plist contains particle indices of stars in object i
        if params['map_type'] == 'image': plist = np.array(pixstars[i])
        if len(plist) == 0: continue
        indices = np.where(np.in1d(saccept,plist))[0]  # find locations of star IDs in plist within the saccept array
        #print len(saccept),len(saccept[plist]),saccept[plist]
        smass[i] = sm[indices]
        spos[i] = sp[indices]
        svlos[i] = sv[indices]
        sage[i] = sa[indices]
        smet[i] = sZtot[indices]
    gmass = [None]*Nobjs
    gpos = [None]*Nobjs
    ghsm = [None]*Nobjs
    gmet = [None]*Nobjs
    gsfr = [None]*Nobjs
    if params['output_type'] == 'gas' or params['output_type'] == 'datacube' or params['output_type'] == 'phys':
        gvlos = [None]*Nobjs
        gneut = [None]*Nobjs
        gmol = [None]*Nobjs
        gnelec = [None]*Nobjs
        gtemp = [None]*Nobjs
    for i in range(Nobjs):        # get the required info for gas particles in halos.  Gives a list of lists, length=Nobjs
        if params['map_type'] == 'object': plist = np.array(objs[i].glist)
        if params['map_type'] == 'image': plist = np.array(pixgas[i])
        if len(plist) == 0: continue
        indices = np.where(np.in1d(gaccept,plist))[0]  # find locations of gas IDs in glist within the gaccept array
        gmass[i] = gm[indices]  # in Mo
        gpos[i] = gp[indices]  # in comov kpc
        ghsm[i] = gh[indices]  # in comov kpc
        gmet[i] = gZtot[indices]  # metal mass frac
        gsfr[i] = gsf[indices]  # in Mo/yr
        if params['output_type'] == 'gas' or params['output_type'] == 'datacube' or params['output_type'] == 'phys':
            gvlos[i] = gv[indices]  # in phys km/s
            gneut[i] = gHI[indices]*gm[indices]  # in Mo
            gmol[i] = gH2[indices]*gm[indices]  # in Mo
            gnelec[i] = gne[indices]*gnh[indices]  # in physical cm^-3
            gtemp[i] = gt[indices]  # in K
    '''

    print('pyloser : Done loading simulation. RAM=%g [t= %g s]'%(py.memory_info()[0]/2.**30,t_elapsed(TINIT)))  # memory usage in GB

    if params['output_type'] == 'gas' or params['output_type'] == 'datacube' or params['output_type'] == 'phys':
        return iobjs,smass,spos,smet,svlos,gmass,gpos,ghsm,gmet,gneut,gmol,gvlos,gsfr,gnelec,gtemp,lumtoflux
    else:
        return iobjs,smass,spos,svlos,sage,smet,gmass,gpos,ghsm,gmet,gsfr,lumtoflux


#######################################
