

#=========================================================
# IMPORT STATEMENTS
#=========================================================

import caesar
from readgadget import *
import sys
import pylab as plt
import numpy as np
import fsps
from astropy.cosmology import FlatLambdaCDM
from astropy import constants as const
import time
import os
import extinction
import h5py
from los_extinction import *
from loadsim import *
from pygas import *

#from scipy.ndimage.filters import gaussian_filter

#=========================================================
# READ IN COMMAND LINE INPUTS
#=========================================================

nproc = 0
if len(sys.argv)==6: 
    script,snapfile,caesarfile,parfile,outfile,nproc = sys.argv
    nproc = int(nproc)
elif len(sys.argv)==5:
    script,snapfile,caesarfile,parfile,outfile = sys.argv
else:
    script,snapfile,caesarfile,parfile = sys.argv
    outfile = "%s_loser.hdf5"%caesarfile[:-5]
#par = __import__(parfile)
#import config as cfg
#cfg.par = par
#for i in dir(cfg.par):
#    try: print(i,eval(i))
#    except: print(i,'not found')

params = {}
with open(parfile) as f:
    for line in f:
        try: 
            line = line.partition('#')[0]
            key = line.split()[0]
            line = line.partition('=')
            val = line[2]
            params[key] = val.strip()
        except:
            continue
params['bands'] = params['bands'].split(',')
#print('Read parameters into dictionary:')
#print(params)

# start overall timer
TINIT = time.time()        
CLIGHT_AA = const.c.to('AA/s').value

#=========================================================
# AUXILIARY ROUTINES
#=========================================================

def tage(cosmo,thubble,a):
    return thubble-cosmo.age(1./a-1).value

def get_tage(cosmo,thubble,tform):
    if nproc>1 or nproc<=0: sa = np.array(Parallel(n_jobs=nproc)(delayed(tage)(cosmo,thubble,tf) for tf in tform))
    else: sa = np.asarray([tage(cosmo,thubble,i) for i in tform])
    return sa

def t_elapsed():
    return np.round(time.time()-TINIT,2)

#=========================================================
# ROUTINES TO COMPUTE SPECTRA AND MAGNITUDES
#=========================================================

# integrate the spectrum over a given bandpass to get AB magnitude
def bandmag(spectrum,fsps_ssp,lumtoflux,b,redshift_spec=True):
    band = fsps.filters.get_filter(b)  # look up characteristics of desired band b
    band_wave = band.transmission[0]
    band_trans = band.transmission[1]

    factor = 1.+params['fsps_zred']
    if not redshift_spec:
        factor = 1.
    wavelengths = fsps_ssp.wavelengths*factor  # redshift the wavelengths when computing apparent magnitudes
    ind = np.where((wavelengths > band_wave[0]) & (wavelengths < band_wave[-1]))[0]  # the index of wavelengths in the band_wave range
    ftrans = np.interp(wavelengths[ind],band_wave,band_trans)  # transmission at that wavelength
    spect = spectrum[ind] * cosmic_extinction(wavelengths[ind],params['fsps_zred'])  # apply cosmic UV extinction from Lya forest if desired
    dnu = CLIGHT_AA/wavelengths[ind[0]:ind[-1]+1] - CLIGHT_AA/wavelengths[ind[0]+1:ind[-1]+2]  # convert to delta-nu
    lum = np.sum(spect * ftrans * dnu)
    res = np.sum(ftrans * dnu)
    
    if lum > 0: return -48.6-2.5*np.log10(lum*lumtoflux/res)  # compute AB magnitudes
    else: return 40.0	# when no flux, set to 40th magnitude

# loop over all bands to compute magnitudes from spectra
def load_mags(spectrum,spectrum_nodust,lumtoflux,bands):
    mags = [None]*int(2*len(bands)+2)
    for i in range(len(bands)):
        # first entry is magnitudes with extinction (list with length Nbands)
        mags[i] = bandmag(spectrum,fsps_ssp,lumtoflux,bands[i])
        # second entry is magnitudes with no extinction (list with length Nbands)
        mags[i+len(bands)] = bandmag(spectrum_nodust,fsps_ssp,lumtoflux,bands[i])
    # third entry in mags is A_V for entire galaxy
    mags[-2] = bandmag(spectrum,fsps_ssp,lumtoflux,'v',redshift_spec=False)-bandmag(spectrum_nodust,fsps_ssp,lumtoflux,'v',redshift_spec=False)      
    # fourth entry in mags is total re-radiated L_FIR in Lsun
    attenuation = spectrum_nodust-spectrum
    lamold = fsps_ssp.wavelengths[0]*(1.+params['fsps_zred'])
    lum = 0
    for j in range(1,len(fsps_ssp.wavelengths)):
        lam = fsps_ssp.wavelengths[j]*(1.+params['fsps_zred'])
        dnu = (CLIGHT_AA/lamold-CLIGHT_AA/lam)
        lum += attenuation[j]*dnu  # tally up the dust-attenuated luminosity; we assume this is L_FIR
        lamold = lam
    mags[-1] = lum
    return mags
#    print(t_elapsed(),mags)

# sum spectra over all stars in an object, compute magnitudes
def calc_mags(smass,sage,smet,A_V,gsfr,Solar,lumtoflux,bands):
    spectrum = np.zeros(len(fsps_ssp.wavelengths))
    spectrum_nodust = np.zeros(len(fsps_ssp.wavelengths))
    if gsfr is None: ssfr = 0.
    else: ssfr = sum(gsfr)/sum(smass)
    for i in range(0,len(smass)):
        fsps_ssp.params["logzsol"] = np.log10(smet[i]/Solar[star_metal_index])  # set metallicity for FSPS in solar units
        spect_star = smass[i]*fsps_ssp.get_spectrum(tage=sage[i])[1]  # compute the FSPS spectrum, this is the spectrum for this star
        spectrum_nodust += spect_star  # this tallies up the unextincted spectrum for all stars
        spectrum += spect_star * apply_extinction(params['extinction_law'],A_V[i],sage[i],ssfr,fsps_ssp.wavelengths)  # extincted spectrum
    mags = load_mags(spectrum,spectrum_nodust,lumtoflux,bands)
    return mags

'''
def gaussian_smooth_variable_width(wave,flux,sigma):
    fsmooth = np.zeros(len(wave))
    for i in range(len(wave)):
        window = np.where((wave>=wave[i]-3*sigma[i])&(wave<=wave[i]+3*sigma[i]))
        fsum = wt = 0.
        for j in window[0]:
            wt += np.exp(-0.5*((wave[j]-wave[i])/sigma[i])**2)
            fsum += flux[j]*wt
        fsmooth[i] = fsum/wt
        if i<10: print i,wave[i],flux[i],fsmooth[i]
    return fsmooth
'''

def calc_spec(smass,sage,smet,A_V,gsfr,Solar,lumtoflux,bands,wavelengths,vlos,vsmooth_fwhm):
    spectrum = np.zeros(len(fsps_ssp.wavelengths))  # original spectrum from FSPS
    spectrum_nodust = np.zeros(len(fsps_ssp.wavelengths))
    myspect = np.zeros(len(wavelengths))  # spectrum smoothed into desired wavelength bins
    myspect_nodust = np.zeros(len(wavelengths))
    sigma = vsmooth_fwhm/2.355/const.c.to('km/s').value*fsps_ssp.wavelengths  # smoothing sigma in wavelength (AA)
    if gsfr is None: ssfr = 0.
    else: ssfr = sum(gsfr)/sum(smass)
    for i in range(0,len(smass)):
        fsps_ssp.params["logzsol"] = np.log10(smet[i]/Solar[star_metal_index])  # set metallicity in SSP
        # get spectrum on FSPS wavelength scale
        spect_star = smass[i]*fsps_ssp.get_spectrum(tage=sage[i])[1]  # compute SSP, multiply by stellar mass
        spectrum_nodust += spect_star  # accumulate for magnitude calculation
        spectrum += spect_star * apply_extinction(params['extinction_law'],A_V[i],sage[i],ssfr,fsps_ssp.wavelengths)
        # get redshifted spectrum on desired wavelength scale
        if vsmooth_fwhm>0: spect_star = fsps_ssp.smoothspec(fsps_ssp.wavelengths,spect_star,vsmooth_fwhm/2.355)  # smooth in velocity space
        mywave = (1+vlos[i]/const.c.to('km/s').value)*fsps_ssp.wavelengths  # redshift each star's spectrum based on LOS vel
        spect_star = np.interp(wavelengths,mywave,spect_star)  # interpolate onto desired wavelengths
        myspect_nodust += spect_star  # accumulate desired spectrum for object
        myspect += spect_star * apply_extinction(params['extinction_law'],A_V[i],sage[i],ssfr,wavelengths)
    # convert spectra to erg/s/cm^2/AA (original spectra are per Hz)
    for i in range(0,len(wavelengths)): 
        spectoAA = lumtoflux*CLIGHT_AA/(wavelengths[i]*wavelengths[i])
        myspect[i] *= spectoAA
        myspect_nodust[i] *= spectoAA
        #if i<5: print np.log10(sum(smass)),wavelengths[i],myspect[i],myspect_nodust[i]
# compute magnitudes -- might as well since it's twiddle free in terms of CPU+diskspace.  this fills first 4 entries in mymags.
    mags = load_mags(spectrum,spectrum_nodust,lumtoflux,bands)
    # fifth entry in mags is spectrum 
    for i in range(0,len(wavelengths)): mags.append(myspect[i])
    # sixth entry in mags is extinction-free spectrum
    for i in range(0,len(wavelengths)): mags.append(myspect_nodust[i])

    return mags


#=========================================================
# DRIVER ROUTINE
#=========================================================

def pyloser(fsps_ssp,snapfile,sim,objs,bands=['v']):

# set up cosmology
    cosmo = FlatLambdaCDM(H0=100*sim.simulation.hubble_constant, Om0=sim.simulation.omega_matter, Ob0=sim.simulation.omega_baryon,Tcmb0=2.73)

# read in particle data
    iobjs,smass,spos,svlos,sage,smet,gmass,gpos,ghsm,gmet,gsfr,lumtoflux = load_sim(snapfile,sim,objs,cosmo,params)
    Nobjs = len(iobjs)
# get original stellar mass at time of formation
    for i in range(Nobjs): 
        smass[i] = smass[i] / np.interp(np.log10(sage[i]*1.e9),fsps_ssp.ssp_ages,fsps_ssp.stellar_mass)  

# compute extinction for all star particles
    #print('pyloser : Starting A_V calculation for nobj=%d ngas=%d nstar=%d [t=%g s]'%(Nobjs,len([i for il in gmass for i in il]),len([i for il in smass for i in il]),t_elapsed()))
    print('pyloser : Starting A_V calculation for nobj=%d [t=%g s]'%(Nobjs,t_elapsed()))
    kerntab = init_kerntab(params['kernel_type'])
    Solar = set_solar()
    if nproc==1:
        A_V = [None]*Nobjs
        for i in range(Nobjs): 
            A_V[i] = compute_AV(int(params['view_dir']),gmass[i],gpos[i],ghsm[i],gmet[i],spos[i],Solar,sim.simulation.boxsize.value,sim.simulation.scale_factor,params['kernel_type'],kerntab)
    else: 
        A_V = Parallel(n_jobs=nproc)(delayed(compute_AV)(int(params['view_dir']),gmass[i],gpos[i],ghsm[i],gmet[i],spos[i],Solar,sim.simulation.boxsize.value,sim.simulation.scale_factor,params['kernel_type'],kerntab) for i in range(Nobjs))

# compute spectra and magnitudes in desired bands
    print('pyloser : Starting spectrum & magnitude calculation for %d objects/pixels [t=%g s]'%(Nobjs,t_elapsed()))
    bands_wavelength = np.zeros(len(bands))
    for i in range(len(bands)):
        band = fsps.filters.get_filter(bands[i])
        print('pyloser : Doing band: %d %s %g %g'%(band.index,band.name,band.lambda_eff,band.msun_ab))
        bands_wavelength[i] = band.lambda_eff
# loop over objects to get desired mags
    if params['output_type'] == 'phot':
        if nproc==1: 
            allmags = [None]*Nobjs
            for i in range(Nobjs): allmags[i] = calc_mags(smass[i],sage[i],smet[i],A_V[i],gsfr[i],Solar,lumtoflux,bands)
        else: 
            allmags = Parallel(n_jobs=nproc)(delayed(calc_mags)(smass[i],sage[i],smet[i],A_V[i],gsfr[i],Solar,lumtoflux,bands) for i in range(Nobjs))
    elif params['output_type'] == 'spec':
        spec_npix = eval(params['spec_npix'])
        dlambda = (eval(params['spec_wave'])[1]-eval(params['spec_wave'])[0])/spec_npix
        wavelengths = np.arange(eval(params['spec_wave'])[0],eval(params['spec_wave'])[1],dlambda,dtype=float)  # set up desired wavelengths
        # compute LOS velocity = hubble flow from x[idir]=0 position plus LOS peculiar velocity
        idir = int(params['view_dir'])
        hubble = cosmo.H(sim.simulation.redshift).value
        vlos = [None]*Nobjs
        for iobj in range(Nobjs): vlos[iobj] = [spos[iobj][i][idir]*1.e-3*hubble + svlos[iobj][i] for i in range(len(spos[iobj]))]
        vlos = np.asarray(vlos)
        # compute magnitudes and spectra
        if nproc==1: 
            allmags = [None]*Nobjs
            for i in range(Nobjs): 
                print 'doing galaxy',i
                mags = calc_spec(smass[i],sage[i],smet[i],A_V[i],gsfr[i],Solar,lumtoflux,bands,wavelengths,vlos[i],eval(params['vsmooth_fwhm']))
                allmags[i] = mags
        else: 
            allmags = Parallel(n_jobs=nproc)(delayed(calc_spec)(smass[i],sage[i],smet[i],A_V[i],gsfr[i],Solar,lumtoflux,bands,wavelengths,vlos[i],eval(params['vsmooth_fwhm'])) for i in range(Nobjs))
    else:
        print("I'm a loser baby: output_type",params['output_type'],"unrecognized.")
    print('pyloser : Done spectrum & magnitude calculation [t=%g s]'%t_elapsed())

# parse and load magnitudes into appropriate arrays (and spectra if requested)
    allmags = np.asarray(allmags)
    if params['output_type'] == 'phot': allmags = np.reshape(allmags,(allmags.size/(2*len(bands)+2),2*len(bands)+2))[:Nobjs]
    if params['output_type'] == 'spec': allmags = np.reshape(allmags,(allmags.size/(2*len(bands)+2+2*spec_npix),2*len(bands)+2+2*spec_npix))[:Nobjs]
    mags = [i[0:len(bands)] for i in allmags]
    mags_nodust = [i[len(bands):2*len(bands)] for i in allmags]
    AV_galaxy = np.asarray([i[2*len(bands)] for i in allmags])
    L_FIR = np.asarray([i[2*len(bands)+1] for i in allmags])
    if params['output_type'] == 'spec': 
        spec = np.asarray([i[2*len(bands)+2:2*len(bands)+2+spec_npix] for i in allmags])
        spec_nodust = np.asarray([i[2*len(bands)+2+spec_npix:2*len(bands)+2+2*spec_npix] for i in allmags])
    else:
        spec = []
        spec_nodust = []
        wavelengths = []
    #print('pyloser : First 3 elements in mags array with shape ',np.shape(mags))
    #print('mags:',mags[:3])

    if params['map_type'] == 'image':         # decode pixel array into list of (x,y) indices
        Npix = eval(params['image_Npix'])
        iobjs = np.column_stack((iobjs%Npix[0],iobjs/Npix[0]))

    return bands_wavelength,mags,mags_nodust,AV_galaxy,L_FIR,iobjs,wavelengths,spec,spec_nodust


#=========================================================
# MAIN ROUTINE
#=========================================================

if __name__ == '__main__':

# Set up multiprocessing
    if nproc == 0: 
        try: nproc = eval(params['nproc'])
        except: nproc = 1
    if nproc != 1:
        import multiprocessing
        from joblib import Parallel, delayed
        from functools import partial
        num_cores = multiprocessing.cpu_count()
        if nproc < 0: print('Using %d cores (all but %d)'%(num_cores+nproc+1,-nproc-1) )
        if nproc > 1: print('Using %d of %d cores'%(nproc,num_cores))
    else: print('Using single core')

# Read in caesar file
    sim = caesar.load(caesarfile) 
    myobjs = eval(params['objs'])  # set the desired objects 

# Option 1: Do photometry or IFU for desired objects, in desired bands
    if params['output_type'] == 'phot' or params['output_type'] == 'spec': 
        # Set up FSPS parameters
        print('pyloser : Generating FSPS SSP [t=%g s]'%t_elapsed())
        if eval(params['fsps_compute_abs_mag']): 
            params['fsps_zred'] = 0        # redshift=0 computes absolute magnitudes
            params['redshift_colors'] = False        
        else: 
            print('Doing apparent mags at z=',sim.simulation.redshift)
            params['fsps_zred'] = sim.simulation.redshift # apparent magnitudes at snapshot redshift
            params['redshift_colors'] = True        
        print('pyloser : Using parameters',params)
        # set which metal to use for SSP -- see set_solar() for list
        star_metal_index = eval(params['star_metal_index'])    
        print('metal_index(gas,star) = (%d,%d)\n'%(eval(params['gas_metal_index']),star_metal_index))
        # create instance of SSP with provided parameters
        fsps_ssp = fsps.StellarPopulation(sfh=eval(params['fsps_sfh']),
            zcontinuous=eval(params['fsps_zcontinuous']),
            imf_type=eval(params['fsps_imf_type']),
            dust_type=eval(params['fsps_dust_type']),
            add_igm_absorption=False,
            add_neb_emission=eval(params['fsps_add_neb_emission']),
            zred=params['fsps_zred'],
            redshift_colors=params['redshift_colors'])
        # Call main routine to compute magnitudes, etc
        bands_wavelength,mymags,mymags_nodust,A_V,L_FIR,iobjs,wavelengths,spec,spec_nodust = pyloser(fsps_ssp,snapfile,sim,myobjs,bands=params['bands'])  
        # output hdf5 file
        with h5py.File(outfile, 'w') as hf:
            for key, value in params.items():
                hf.attrs.create(key, value, dtype=h5py.special_dtype(vlen=str)) # special_dtype required (see https://github.com/h5py/h5py/issues/289#issuecomment-329853651)
            hf.create_dataset('iobjs',data=iobjs)
            hf.create_dataset('mymags_wavelengths',data=bands_wavelength)
            hf.create_dataset('mymags',data=mymags)
            hf.create_dataset('mymags_nodust',data=mymags_nodust)
            hf.create_dataset('A_V',data=A_V)
            hf.create_dataset('L_FIR',data=L_FIR)
            if params['output_type'] == 'spec': 
                hf.create_dataset('myspec_wavelengths',data=wavelengths)
                hf.create_dataset('myspec',data=spec)
                hf.create_dataset('myspec_nodust',data=spec_nodust)

# Option 2: Do gas and other physical quantities
    elif params['output_type'] == 'gas' or params['output_type'] == 'datacube' or params['output_type'] == 'phys':
        # generate data cube for desired quantities
        iobjs,physmap,physcube = pygas(snapfile,sim,myobjs,params,nproc)
        # output hdf5 file
        quants = params['bands']
        print("pyloser : Outputting %s to hdf5 file %s [t=%g s]"%(quants,outfile,t_elapsed()))
        with h5py.File(outfile, 'w') as hf:
            for key, value in params.items():
                hf.attrs.create(key, value, dtype=h5py.special_dtype(vlen=str))
            hf.create_dataset('iobjs',data=iobjs)
            hf.create_dataset('physmap',data=physmap)
            if params['output_type'] == 'datacube':
                for i in range(len(quants)):
                    hf.create_dataset(quants[i],data=physcube[i])
    else:
        print('pyloser : Output_type %s not recognized'%(params['output_type']))

    print('pyloser : Soy un perdedor, adios! [t=%g s]'%t_elapsed())

