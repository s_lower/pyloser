
import numpy as np
import time
import sys
import multiprocessing
from joblib import Parallel, delayed
from functools import partial

#=========================================================
# READ IN COMMAND LINE INPUTS
#=========================================================

def parse_args(sysargs):
    nproc = 0
    if len(sysargs)==6: 
        script,snapfile,caesarfile,parfile,outfile,nproc = sysargs
        nproc = int(nproc)
    elif len(sysargs)==5:
        script,snapfile,caesarfile,parfile,outfile = sysargs
    else:
        script,snapfile,caesarfile,parfile = sysargs
        outfile = "%s_loser.hdf5"%caesarfile[:-5]
    #par = __import__(parfile)
    #import config as cfg
    #cfg.par = par
    #for i in dir(cfg.par):
    #    try: print(i,eval(i))
    #    except: print(i,'not found')
    params = {}
    with open(parfile) as f:
        for line in f:
            try: 
                line = line.partition('#')[0]
                key = line.split()[0]
                line = line.partition('=')
                val = line[2]
                params[key] = val.strip()
            except:
                continue
    params['bands'] = params['bands'].split(',')
    if len(sysargs)<6: 
      try: nproc = eval(params['nproc'])
      except: nproc = 1  # defaults to single core
      print('Setting nproc=%d'%nproc)
    #print('Read parameters into dictionary:')
    #print(params)
    return snapfile,caesarfile,parfile,outfile,nproc,params

#=========================================================
# COSMOLOGY ROUTINES
#=========================================================

from astropy.cosmology import FlatLambdaCDM
from astropy import constants as const

def init_cosmo(sim):  # assume LCDM
    global Omratio,H0,thubble_fact,thubble
    H0 = 100*sim.simulation.hubble_constant
    cosmo = FlatLambdaCDM(H0=H0, Om0=sim.simulation.omega_matter, Ob0=sim.simulation.omega_baryon,Tcmb0=2.73)
    Omratio = sim.simulation.omega_matter/(1.-sim.simulation.omega_matter)
    thubble0 = cosmo.age(0).value
    thubble_fact = thubble0 / ( np.log(1.+np.sqrt(1+Omratio)) - 0.5*np.log(Omratio) )
    a = sim.simulation.scale_factor
    thubble = thubble0 * ( np.log(1.+np.sqrt(1+Omratio/(a*a*a))) + 1.5*np.log(a) - 0.5*np.log(Omratio)) / ( np.log(1.+np.sqrt(1+Omratio)) - 0.5*np.log(Omratio) )
    return cosmo

# The formula for lookback time and H(Z) come from Hans de Ruiter's "Distances, Volumes, Time Scales in Cosmology" writeup (google "de Ruiter cosmological formulas").  Or you can derive them yourself! :)

def hubble_z(sim):
    #cosmo = FlatLambdaCDM(H0=H0, Om0=sim.simulation.omega_matter, Ob0=sim.simulation.omega_baryon,Tcmb0=2.73)
    z = sim.simulation.redshift
    H_z = H0 * np.sqrt((1.+Omratio*(1+z)*(1+z)*(1+z))/(1.+Omratio))
    return H_z

def get_tage(a):  # gives age in Gyr of star formed at expansion factor a, relative to current hubble time.
    sa = thubble - thubble_fact * ( np.log(1.+np.sqrt(1+Omratio/(a*a*a))) + 1.5*np.log(a) - 0.5*np.log(Omratio))
    return sa

def get_lumtoflux(cosmo,redshift):
    lumdist = cosmo.luminosity_distance(redshift).to('pc').value
    lumtoflux = const.L_sun.to('erg/s').value/(4* np.pi * lumdist**2 * const.pc.to('cm').value**2)
    return lumtoflux

#=========================================================
# TIMER ROUTINES
#=========================================================

def t_elapsed(TINIT):
    return np.round(time.time()-TINIT,2)

# progress bar, from https://stackoverflow.com/questions/3160699/python-progress-bar
def progress_bar(progress,barLength=10,t=None):
    status = ""
    if isinstance(progress, int):
        progress = float(progress)
    if not isinstance(progress, float):
        progress = 0
        status = "error: progress var must be float\r\n"
    if progress < 0:
        progress = 0
        status = "Halt...\r\n"
    if progress >= 1:
        progress = 1
        status = "Done!\r\n"
    block = int(round(barLength*progress))
    if t==None: text = "\r[{0}] {1}% {2}".format( "#"*block + "-"*(barLength-block), np.round(progress*100,2), status)
    else: text = "\r[{0}] {1}% [t={2} s] {3}".format( "#"*block + "-"*(barLength-block), np.round(progress*100,2), np.round(t,2), status)
    sys.stdout.write(text)
    sys.stdout.flush()

"""
def tage(cosmo,thubble,a):
    A = cosmo.Om0/cosmo.Ode0
    tlookback = 1. - ( np.log(1.+np.sqrt(1+A/(a*a*a))) + 1.5*np.log(a) - 0.5*np.log(A)) / ( np.log(1.+np.sqrt(1+A)) - 0.5*np.log(A) )
    print(1./a-1,thubble-cosmo.age(1./a-1).value,thubble,tlookback,thubble-cosmo.age(0).value*(1.-tlookback),cosmo.age(1./a-1).value-cosmo.age(0).value*(1.-tlookback))
    return thubble-cosmo.age(1./a-1).value
"""

