#!/bin/bash

snapfile=$1
caesarfile=$2
gal_ind=$3
pixelscale=$4

outfile_param=param_image_gal$gal_ind
outfile_pyloser=loser_gal$gal_ind.hdf5

python gen_image_params.py $caesarfile $gal_ind $pixelscale $outfile_param
python pyloser.py $snapfile $caesarfile $outfile_param $outfile_pyloser

rm $outfile_param

