
import numpy as np
from astropy.cosmology import FlatLambdaCDM
#from astropy import constants as const
import time
import os
from los_extinction import set_solar
import loadsim 
import multiprocessing
from joblib import Parallel, delayed
from functools import partial
import astropy.constants as const

# Sums up masses in given arrays for desired quantities
def compute_obj_mass(gmass,gpos,ghsm,gmet,gneut,gmol,gsfr,gnelec,gtemp,smass,spos,Solar,params):
    quants = params['bands']
    qvals = np.zeros(len(quants))
    for i in range(len(quants)):
        if smass is not None: 
            if quants[i] == 'Mstar': qvals[i] = sum(smass)  
        if gmass is not None: 
            if quants[i] == 'Mgas': qvals[i] = sum(gmass)   # baryonic mass (Mo)
            if quants[i] == 'SFR': qvals[i] = sum(gsfr)   # SFR (Mo/yr)
            if quants[i] == 'Z': qvals[i] = sum(gmass*gmet)/sum(gmass)   # mean metal mass fraction
            if quants[i] == 'HI': qvals[i] = sum(gneut)  # HI mass (Mo)
            if quants[i] == 'H2': qvals[i] = sum(gmol)   # H2 mass (Mo)
            if quants[i] == 'tSZ': qvals[i] = sum(gnelec*gtemp)   # H2 mass (Mo)
    return qvals

# fills a velocity-space spectrum with for an object's quantities q having velocities vlos
def compute_spec(q,vlos,npix,dvel,v0):
    spec = np.zeros(npix)  # spectrum for this object
    if q is not None:
        for i in range(len(q)):  # loop over particles within a given object
            ivel = int((vlos[i]-v0)/dvel)  # determine velocity bin for that particle
            if ivel >= npix or ivel<0: continue   # velocity is outside of desired range; ignore
            #print vlos[i],dvel,ivel
            spec[ivel] += q[i]  # sum up quantity in that vel bin
    return spec

# Driver routine to sum up masses in object, and compute velocity datacube if desired
def compute_datacube(gmass,gpos,ghsm,gmet,gneut,gmol,gvel,gsfr,gnelec,gtemp,smass,spos,svel,Solar,Lbox,ascale,params):
    allspec = []
    # compute total masses in each object
    allspec.append(compute_obj_mass(gmass,gpos,ghsm,gmet,gneut,gmol,gsfr,gnelec,gtemp,smass,spos,Solar,params))
    tSZconst = (const.k_B*const.sigma_T/(const.m_e*const.c*const.c)).to('cm2 / K').value
    if params['output_type'] == 'datacube':  # option to compute velocity datacubes
        quants = params['bands']
        # set up desired velocities
        spec_npix = eval(params['spec_npix'])
        vstart = eval(params['spec_vel'])[0]
        dvel = (eval(params['spec_vel'])[1]-vstart)/spec_npix
        for i in range(len(quants)):  # find spectrum for each desired quantity
            spec = np.zeros(spec_npix)  # spectrum for this object
            if quants[i] == 'Mstar': spec = compute_spec(smass,svel,spec_npix,dvel,vstart)
            if gmass is not None: 
                if quants[i] == 'Mgas': spec = specmass = compute_spec(gmass,gvel,spec_npix,dvel,vstart)   
                if quants[i] == 'SFR': spec = compute_spec(gsfr,gvel,spec_npix,dvel,vstart)
                if quants[i] == 'Z': 
                    spec = compute_spec(gmet*gmass,gvel,spec_npix,dvel,vstart)
                    spec /= (specmass+1.e-10)
                if quants[i] == 'HI': spec = compute_spec(gneut,gvel,spec_npix,dvel,vstart)
                if quants[i] == 'H2': spec = compute_spec(gmol,gvel,spec_npix,dvel,vstart)
                if quants[i] == 'tSZ': 
                    spec = compute_spec(gnelec*gtemp,gvel,spec_npix,dvel,vstart)
                    spec *= tSZconst
            allspec.append(spec)  # append spectrum for each quantity
    return allspec  # returns spectra of various quantities for this object

#=========================================================
# DRIVER ROUTINE TO MAKE MAPS OF GAS AND PHYSICAL QUANTITIES
#=========================================================

def pygas(snapfile,sim,objs,params,nproc):
# set up cosmology
    cosmo = FlatLambdaCDM(H0=100*sim.simulation.hubble_constant, Om0=sim.simulation.omega_matter, Ob0=sim.simulation.omega_baryon,Tcmb0=2.73)
    hubble = cosmo.H(sim.simulation.redshift).value * 1.e-3  # in km/s/kpc; to get physical km/s, positions must be in phys kpc

# read in particle data
    iobjs,smass,spos,smet,svlos,gmass,gpos,ghsm,gmet,gneut,gmol,gvlos,gsfr,gnelec,gtemp,lumtoflux = loadsim.load_sim(snapfile,sim,objs,cosmo,params)
    Nobjs = len(iobjs)
    idir = int(params['view_dir'])
    vbox = sim.simulation.boxsize.value*sim.simulation.scale_factor*hubble
    try:
        if eval(params['zero_peculiar_velocity']):
            for iobj in range(Nobjs): 
                if svlos[iobj] is not None: svlos[iobj] = 0.*svlos[iobj]
                if gvlos[iobj] is not None: gvlos[iobj] = 0.*gvlos[iobj]
    except:
        sv0 = 0

# set velocity zeropoint 
    sv0 = m0 = 0
    svel = [0]*Nobjs
    for iobj in range(Nobjs): 
        if svlos[iobj] is not None:
            svel[iobj] = [spos[iobj][i][idir]*hubble*sim.simulation.scale_factor + svlos[iobj][i] for i in range(len(svlos[iobj]))]
            sv0 += sum(svel[iobj]*smass[iobj])
            m0 += sum(smass[iobj])
    if m0 > 0:  sv0 /= m0 # set to mean stellar velocity
    sv0 = 0.  # set to zero, so user must specify desired velocity range for object

# compute LOS velocity relative to velocity zeropoint, including hubble flow plus peculiar vel
    gvel = [None]*Nobjs
    for iobj in range(Nobjs):
        if gmass[iobj] is not None: 
            gvel[iobj] = [gpos[iobj][i][idir]*hubble*sim.simulation.scale_factor for i in range(len(gvlos[iobj]))] # hubble flow
            for i in range(len(gvlos[iobj])):
                if gvel[iobj][i] > 0.5*vbox: gvel[iobj][i] -= vbox  # periodically wrap hubble flow
                if gvel[iobj][i] < -0.5*vbox: gvel[iobj][i] += vbox
                gvel[iobj][i] += gvlos[iobj][i] - sv0  # now add peculiar velocity and normalize to velocity zeropoint

# compute gas properties for all objects
    #print('pyloser : Starting gas calculation for nobj=%d [t=%g s]'%(Nobjs,t_elapsed()))
    Solar = set_solar()
    if nproc==1:
        datacube = [None]*Nobjs
        for i in range(Nobjs):
            datacube[i] = compute_datacube(gmass[i],gpos[i],ghsm[i],gmet[i],gneut[i],gmol[i],gvel[i],gsfr[i],gnelec[i],gtemp[i],smass[i],spos[i],svel[i],Solar,sim.simulation.boxsize.value,sim.simulation.scale_factor,params)
    else:
        datacube = Parallel(n_jobs=nproc)(delayed(compute_datacube)(gmass[i],gpos[i],ghsm[i],gmet[i],gneut[i],gmol[i],gvel[i],gsfr[i],gnelec[i],gtemp[i],smass[i],spos[i],svel[i],Solar,sim.simulation.boxsize.value,sim.simulation.scale_factor,params) for i in range(Nobjs))
    physmap = [datacube[i][0] for i in range(len(datacube))]
    quants = params['bands']
    physcube = [None]*len(quants)
    if params['output_type'] == 'datacube':  # option to compute velocity datacubes
        for j in range(len(quants)):
            physcube[j] = [datacube[i][j+1] for i in range(len(datacube))]
    assert(len(physmap)==len(physcube[0]))

# make sure the output mode is correct for image map types
    if params['map_type'] == 'image':         # decode pixel array into list of (x,y) indices
        Npix = eval(params['image_Npix'])
        iobjs = np.column_stack((iobjs%Npix[0],iobjs/Npix[0]))

    return iobjs,physmap,physcube
