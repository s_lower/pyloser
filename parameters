
# Parameter file for pyloser.
# IMPORTANT: There must not be any spaces in your parameter values! line.split() will get confused!

# direction of viewing: 0=x, 1=y, 2=z
view_dir = 0
# extinction law from http://extinction.readthedocs.io/en/latest/: ccm89, calzetti00, fm07
# plus extra options: salmon16=calzetti+power law in A_V (for hi-z);  cf00_calzetti=calzetti+power law for age<3e7
# see los_extinction.py for exact computation
#extinction_law = salmon16
#extinction_law = ccm89
extinction_law = mix_calz00_fm07

# acceptance criteria for particles to be dust-obscuring: gm=mass, gZtot=metallicity, gt=temperature, gnh=n_H(cgs), etc.
gas_crit = ((gd>0)&(gt<1e6)&(gnh>0.01))  # reasonable choices

# type of map: object (e.g. galaxies or halos) or image (specify number of pixels below) 
map_type = object
# for 'object' need an input Caesar object: e.g. sim.galaxies, sim.galaxies[:10], sim.halos[4,6,9]
objs = sim.galaxies
# for 'image' need a center, size, and Npixels.  Center & size are in 3-D, to allow cuts along LOS.  All values in COMOVING KPC.
# center can be values e.g. [5000,5000,5000], or reference caesar objects e.g. sim.galaxies[5].pos
image_center = sim.galaxies[0].pos
image_size = [-1,50,50]	# total extent of image, in comoving kpc; if view_dir component is negative, then use entire box along LOS
image_Npix = [50,50]	# number of pixels in image

# type of output: phot (photometry) or spec (spectrum for each galaxy/object; ALSO does photometry)
output_type = spec
# for 'phot' or 'spec', need bands: (to see choices, import fsps and type fsps.find_filter('')
#bands = u,b,v,galex_nuv,sdss_u,sdss_g,sdss_r,sdss_i,sdss_z,2mass_j,2mass_h,2mass_ks,irac_1,irac_2
bands = u,b,v,galex_nuv,sdss_u,sdss_g,sdss_r,sdss_i,sdss_z,wfcam_y,wfcam_j,wfcam_h,wfcam_k,irac_1,irac_2,wfc3_uvis_f275w,wfc3_uvis_f438w,wfc3_uvis_f606w,wfc3_uvis_f814w,wfc3_ir_f105w,wfc3_ir_f125w,wfc3_ir_f160w,suprimecam_b,suprimecam_v,suprimecam_g,suprimecam_r_uds,suprimecam_i,suprimecam_z,vista_y,jwst_f115w,jwst_f150w,jwst_f200w,jwst_f277w,jwst_f356w,jwst_f444w,LSST_u,LSST_g,LSST_r,LSST_i,LSST_y,LSST_z
# for 'spec', need to specify spectrum parameters
spec_wave = [4000,9000]		# wavelength range in Angstroms (rest frame)
spec_npix = 5000		# number of spectral pixels
vsmooth_fwhm = 10.0		# FWHM for Gaussian smoothing in km/s

# set up metallicities to use: for Gizmo-mufasa, 0=Total, 2=C, 3=N, 4=O, 5=Ne, 6=Mg, 7=Si, 8=S, 9=Ca, 10=Fe
gas_metal_index = -1    # element used to compute LOS extinction by converting this metal abundance to A_V; should *really* be 0 (total) since calibrations tend to be based on this.
star_metal_index = 0   # element used to compute SSP, scaled to solar.  Usually 0 for total, or 10 for Fe.

# generate or use pre-computed lookup table for SSP spectra; if it doesn't exist, file will be created for future use.  comment out to not use this.
ssp_lookup_table = SSP_Chab_EL.hdf5  

# set up Python-FSPS parameters;  see http://dfm.io/python-fsps/current/stellarpop_api
fsps_imf_type = 1		# 0=Salpeter, 1=Chabrier, 2=Kroupa
fsps_add_neb_emission = True	# Note: Useful for 'spec' mode, but turning this on significantly increases run time

# smoothing kernel type: cubic or quintic; Gizmo-mufasa uses cubic
kernel_type = cubic

# Things you probably shouldn't mess with
fsps_sfh = 0     # makes python-fsps do an SSP
fsps_zcontinuous = 1     # Use individual star metallicities
fsps_dust_type = 2		# irrelevant, but needs to be something

# number of processors to use (can be overridden by command-line argument)
nproc = 1

