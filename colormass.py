

#=======================================================================
# Generates a color-mass diagram from pyloser hdf5 file (and caesar file)
#=======================================================================

import caesar
import pylab as plt
import numpy as np
import h5py
import sys
from load_pyloser import get_mags

script,caesarfile,loserfile,band1,band2 = sys.argv

if __name__ == '__main__':
    sim = caesar.load(caesarfile,LoadHalo=False) # load caesar file
    # input pyloser file
    params,mymags = get_mags(loserfile,'absmag',bandlist=[band1,band2])
    bands = params['bands']
    iband1 = np.where(bands==band1)[0][0]
    iband2 = np.where(bands==band2)[0][0]
    print('Doing color',bands[iband1],'-',bands[iband2])

    # get caesar info for these objects
    myobjs = eval(params['objs'])
    logms = np.asarray([np.log10(i.masses['stellar']) for i in myobjs])
    sfr = np.asarray([i.sfr for i in myobjs])
    ssfr = np.log10(1.e9*sfr/10**logms+10**(-2.5+0.3*sim.simulation.redshift))

    # plot stuff
    fig,ax = plt.subplots()
    pixcolor = ssfr
    pixsize = logms-min(logms)+1
    im = ax.scatter(logms,mymags[0]-mymags[1], c=pixcolor, s=pixsize, lw=0, cmap=plt.cm.jet_r,label=r'$z=%.1g$'%sim.simulation.redshift)
    im.set_clim(-2.7+0.5*sim.simulation.redshift,0.5*sim.simulation.redshift)
    fig.colorbar(im,ax=ax,label=r'$sSFR$ Gyr$^{-1}$')
    plt.minorticks_on()
    #ax.set_ylim(ax.get_ylim()[::-1])	# invert y axis, if desired eg for a mass-magnitude diagram
    plt.ylabel(r'%s - %s'%(bands[iband1],bands[iband2]),fontsize=16)
    plt.xlabel(r'$log M_{*}$' ,fontsize=16)
    plt.legend(loc='lower right')

    #plt.savefig('colormass.pdf', bbox_inches='tight', format='pdf')
    plt.show()


#######################################


