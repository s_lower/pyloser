

#=========================================================
# IMPORT STATEMENTS
#=========================================================

import caesar
from readgadget import *
import sys
#import pylab as plt
import numpy as np
import fsps
from astropy import constants as const
import time
import os
import extinction
import h5py
from los_extinction import *
from loadsim import *
from ssp_lookup import *
from pygas import *
from auxloser import t_elapsed,parse_args,progress_bar,hubble_z

#from scipy.ndimage.filters import gaussian_filter

# start overall timer
TINIT = time.time()        
CLIGHT_AA = const.c.to('AA/s').value
lumtoflux_abs = const.L_sun.to('erg/s').value/(4* np.pi * 10.**2 * const.pc.to('cm').value**2)


#=========================================================
# ROUTINES TO COMPUTE SPECTRA AND MAGNITUDES
#=========================================================

# define class holding band information
class band_data:
    def __init__(self,name,indices,trans,dnu,wavelength):
        self.name = name  # name in FSPS
        self.indices = indices  # indices covering band in fsps_ssp.wavelengths
        self.trans = trans  # transmission interpolated to fsps_ssp.wavelengths[indices]
        self.dnu = dnu  # conversion factor from delta-lambda to delta-nu
        self.wavelength = wavelength  # transmission-weighted mean wavelength

# initialize band transmission data interpolated to FSPS wavelengths
def init_bands(band_names,wavelengths):
    bands = []  # list of class instances
    meanwave = np.zeros(len(band_names))
    for i in range(len(band_names)):
        band = fsps.filters.get_filter(band_names[i])  # look up characteristics of desired band 
        band_wave = band.transmission[0]   # filter wavelengths
        band_trans = band.transmission[1]  # filter response function
        ind = np.where((wavelengths > band_wave[0]) & (wavelengths < band_wave[-1]))[0]  # indices of wavelengths in the band
        ftrans = np.interp(wavelengths[ind],band_wave,band_trans)  # transmission at those wavelengths
        dnu = CLIGHT_AA/wavelengths[ind[0]:ind[-1]+1] - CLIGHT_AA/wavelengths[ind[0]+1:ind[-1]+2]  # convert to delta-nu
        meanwave[i] = np.sum(band.transmission[0]*band.transmission[1])/np.sum(band.transmission[1])
        bands.append(band_data(band_names[i],ind,ftrans,dnu,meanwave[i]))

        print('pyloser : Doing band %d: %s  wavelength= %g'%(band.index,band.name,meanwave[i]))
    return bands,meanwave

# integrate the spectrum over a given bandpass to get AB magnitude
def bandmag(spectrum,wavelengths,lumtoflux,b,redshift,atten_curves):
    #band = fsps.filters.get_filter(b)  # look up characteristics of desired band b
    #band_wave = band.transmission[0]   # filter wavelengths
    #band_trans = band.transmission[1]  # filter response function
    #ind = np.where((wavelengths > band_wave[0]) & (wavelengths < band_wave[-1]))[0]  # indices of wavelengths in the band
    #ftrans = np.interp(wavelengths[ind],band_wave,band_trans)  # transmission at those wavelengths
    ind = b.indices
    ftrans = b.trans
    dnu = b.dnu
    spect = spectrum[ind]*1.  # spectrum in those wavelengths
    if redshift > 0.001: spect *= atten_curves[-1][ind]  # apply cosmic extinction if not computing abs mags (i.e. z=0)
    #dnu = CLIGHT_AA/wavelengths[ind[0]:ind[-1]+1] - CLIGHT_AA/wavelengths[ind[0]+1:ind[-1]+2]  # convert to delta-nu
    lum = np.sum(spect * ftrans * dnu)  # response-weighted luminosity
    res = np.sum(ftrans * dnu)  # weighting of filter response 
    if lum > 0 and res > 0: 
        if redshift == 0: 
            return -48.6-2.5*np.log10(lum*lumtoflux_abs/res)  # absolute AB magnitudes 
        else:
            return -48.6-2.5*np.log10(lum*lumtoflux/res)  # apparent AB magnitudes 
    else: return 50.0	# when no flux, set to 50th magnitude

# loop over all bands to compute magnitudes from spectra
def load_mags(spectrum,spectrum_nodust,lumtoflux,bands,redshift,atten_curves):
    mags = [None]*int(4*len(bands)+2)  # set up mags list to be filled
    for i in range(len(bands)):
        # first entries hold absolute magnitudes with/without extinction, and apparent mags with/without extinction
        mags[i] = bandmag(spectrum,fsps_ssp.wavelengths,lumtoflux,bands[i],0.,atten_curves)
        mags[i+len(bands)] = bandmag(spectrum_nodust,fsps_ssp.wavelengths,lumtoflux,bands[i],0.,atten_curves)
        mags[i+2*len(bands)] = bandmag(spectrum,fsps_ssp.wavelengths*(1+redshift),lumtoflux,bands[i],redshift,atten_curves)
        mags[i+3*len(bands)] = bandmag(spectrum_nodust,fsps_ssp.wavelengths*(1+redshift),lumtoflux,bands[i],redshift,atten_curves)
    # next entry in mags is A_V for entire galaxy
    vband = [b for b in bands if b.name=='v'][0]  # 'v' must appear at least once in band list
    mags[-2] = bandmag(spectrum,fsps_ssp.wavelengths,lumtoflux,vband,0.,atten_curves) - bandmag(spectrum_nodust,fsps_ssp.wavelengths,lumtoflux,vband,0.,atten_curves)      
    # next entry in mags is total re-radiated L_FIR in Lsun
    attenuation = spectrum_nodust-spectrum
    nwave = len(fsps_ssp.wavelengths)
    lam = fsps_ssp.wavelengths*(1.+redshift)
    dnu = np.zeros(nwave)
    lamold = fsps_ssp.wavelengths[0]*(1.+redshift)
    dnu = np.array([CLIGHT_AA/lam[max(j-1,0)]-CLIGHT_AA/lam[j] for j in range(0,nwave)])
    lum = np.sum(attenuation*dnu)  # tally up the dust-attenuated luminosity; we assume this is L_FIR
    mags[-1] = lum
    return mags

# compute spectrum for a given star on FSPS wavelength scale
def compute_star_spectrum(mass,met,age,Zsol,dt,nbinmax,ssp_interpolate):
    # set up age spread for very young stars, by treating a single star as nbin SSPs spanning a time interval dt
    if dt > 2*age: dt = 2*age   # for very small ages, shrink down age spread
    if age <= 0:
        print('pyloser : WARNING! found unphysical age=%g for a particle; setting to 1 Myr'%age)
        age = 0.001
    nbin = np.clip(int(nbinmax*dt/age),1,nbinmax) # scale number of bins inversely with age, so that young stars get split into more SSPs
    agebin = np.array([age+(0.5+i)*dt/nbin-0.5*dt for i in range(nbin)]) # this is the age for this SSP
    spect_star = np.zeros(len(fsps_ssp.wavelengths))
    if ssp_interpolate == None:
        for i in range(nbin):
            fsps_ssp.params["logzsol"] = np.log10(met/Zsol)  # set metallicity for FSPS in solar units
            spect_star += mass * fsps_ssp.get_spectrum(tage=agebin[i])[1]  # sum up the SSPs
    else: 
        for i in range(nbin):
            spect_star += mass * interpolate_ssp_table(fsps_ssp,agebin[i],met,ssp_interpolate)  # use lookup table to interpolate to the desired SSP
    spect_star = spect_star/nbin  # normalize to get the average spectrum for this star over all sub-SSPs
    return spect_star

# sum spectra over all stars in an object, compute magnitudes
def calc_mags(smass,sage,smet,A_V,gsfr,Solar,lumtoflux,bands,ssp_interpolate,redshift,atten_curves):
    spectrum = np.zeros(len(fsps_ssp.wavelengths))
    spectrum_nodust = np.zeros(len(fsps_ssp.wavelengths))
    if len(gsfr) == 0: ssfr = 0.
    else: ssfr = np.clip(np.log10(sum(gsfr)/sum(smass)+1.e-20)-9.,0.,1.)
    try: 
        dt = eval(params['agespread_dt'])  # spread SF age uniformly over dt (in Gyr), centered on true age, using max nbin SSPs
        nbinmax = eval(params['agespread_nbinmax'])
    except:
        dt = 0.
        nbinmax = 1
    for i in range(0,len(smass)):
        spect_star = compute_star_spectrum(smass[i],smet[i],sage[i],Solar[star_metal_index],dt,nbinmax,ssp_interpolate)
        spectrum_nodust += spect_star  # this tallies up the unextincted spectrum for all stars
        spectrum += spect_star * apply_extinction(params['extinction_law'],A_V[i],sage[i],ssfr,fsps_ssp.wavelengths,atten_curves)  # extincted spectrum
    mags = load_mags(spectrum,spectrum_nodust,lumtoflux,bands,redshift,atten_curves)
    return mags

# calculate magnitudes for an individual star particle.  smass, etc are scalars.
def calc_mags_star(smass,sage,smet,A_V,ssfr,Solar,lumtoflux,bands,ssp_interpolate,redshift,atten_curves):
    try: 
        dt = eval(params['agespread_dt'])  # spread SF age uniformly over dt (in Gyr), centered on true age, using max nbin SSPs
        nbinmax = eval(params['agespread_nbinmax'])
    except:
        dt = 0.
        nbinmax = 1
    spectrum_nodust = compute_star_spectrum(smass,smet,sage,Solar[star_metal_index],dt,nbinmax,ssp_interpolate)
    spectrum = spectrum_nodust * apply_extinction(params['extinction_law'],A_V,sage,ssfr,fsps_ssp.wavelengths,atten_curves)  # extincted spectrum
    mags = load_mags(spectrum,spectrum_nodust,lumtoflux,bands,redshift,atten_curves)
    return mags

'''
def gaussian_smooth_variable_width(wave,flux,sigma):
    fsmooth = np.zeros(len(wave))
    for i in range(len(wave)):
        window = np.where((wave>=wave[i]-3*sigma[i])&(wave<=wave[i]+3*sigma[i]))
        fsum = wt = 0.
        for j in window[0]:
            wt += np.exp(-0.5*((wave[j]-wave[i])/sigma[i])**2)
            fsum += flux[j]*wt
        fsmooth[i] = fsum/wt
        if i<10: print i,wave[i],flux[i],fsmooth[i]
    return fsmooth
'''

def calc_spec(smass,sage,smet,A_V,gsfr,Solar,lumtoflux,bands,wavelengths,vlos,vsmooth_fwhm,ssp_interpolate,redshift,atten_curves):
    spectrum = np.zeros(len(fsps_ssp.wavelengths))  # original spectrum from FSPS
    spectrum_nodust = np.zeros(len(fsps_ssp.wavelengths))
    myspect = np.zeros(len(wavelengths))  # spectrum smoothed into desired wavelength bins
    myspect_nodust = np.zeros(len(wavelengths))
    #sigma = vsmooth_fwhm/(2.355*const.c.to('km/s').value)*fsps_ssp.wavelengths  # smoothing sigma in wavelength (AA)
    if len(gsfr) == 0: ssfr = 0.
    else: ssfr = np.clip(np.log10(sum(gsfr)/sum(smass)+1.e-20)-9.,0.,1.)
    try: 
        dt = eval(params['agespread_dt'])  # spread SF age uniformly over dt (in Gyr), centered on true age, using max nbin SSPs
        nbinmax = eval(params['agespread_nbinmax'])
    except:
        dt = 0.
        nbinmax = 1
    for i in range(0,len(smass)):
        spect_star = compute_star_spectrum(smass[i],smet[i],sage[i],Solar[star_metal_index],dt,nbinmax,ssp_interpolate)
        spect_ext = spect_star * apply_extinction(params['extinction_law'],A_V[i],sage[i],ssfr,fsps_ssp.wavelengths,atten_curves)  # apply dust extinction
        #fsps_ssp.params["logzsol"] = np.log10(smet[i]/Solar[star_metal_index])  # set metallicity in SSP
        #spect_star = smass[i]*fsps_ssp.get_spectrum(tage=sage[i])[1]  # compute SSP, multiply by stellar mass
        spectrum_nodust += spect_star  # accumulate for magnitude calculation
        spectrum += spect_ext
        # get redshifted spectrum on desired wavelength scale
        if vsmooth_fwhm>0: 
            spect_star = fsps_ssp.smoothspec(fsps_ssp.wavelengths,spect_star,vsmooth_fwhm*0.424628)  # smooth in velocity space
            spect_ext = fsps_ssp.smoothspec(fsps_ssp.wavelengths,spect_ext,vsmooth_fwhm*0.424628)  # smooth in velocity space
        mywave = (1+vlos[i]/const.c.to('km/s').value)*fsps_ssp.wavelengths  # redshift each star's spectrum based on LOS vel
        spect_star = np.interp(wavelengths,mywave,spect_star)  # interpolate onto desired wavelengths
        spect_ext = np.interp(wavelengths,mywave,spect_ext)  # interpolate onto desired wavelengths
        myspect_nodust += spect_star  # accumulate desired spectrum for object
        myspect += spect_ext
    # convert spectra to erg/s/cm^2/AA (original spectra are per Hz)
    spectoAA = lumtoflux_abs*CLIGHT_AA/(wavelengths*wavelengths)
    myspect *= spectoAA
    myspect_nodust *= spectoAA
# compute magnitudes -- might as well since it's twiddle free in terms of CPU+diskspace.  this fills first 4 entries in mymags.
    mags = load_mags(spectrum,spectrum_nodust,lumtoflux,bands,redshift,atten_curves)
    # fifth entry in mags is spectrum 
    for i in range(0,len(wavelengths)): mags.append(myspect[i])
    # sixth entry in mags is extinction-free spectrum
    for i in range(0,len(wavelengths)): mags.append(myspect_nodust[i])
    #print(t_elapsed(TINIT),mags[0:len(bands)],mags[2*len(bands):3*len(bands)])

    return mags


#=========================================================
# DRIVER ROUTINE
#=========================================================

def pyloser(fsps_ssp,mass_remaining,ssp_interpolate,snapfile,sim,objs,nproc,bands):

# set up list of attenuation laws we will need, including IGM extinction at our given redshift
    atten_curves = setup_extinction(fsps_ssp.wavelengths)
    redshift = sim.simulation.redshift
    atten_curves.append(cosmic_extinction(fsps_ssp.wavelengths*(1.+redshift),redshift)) # IGM attenuation

# read in particle data
    iobjs,smass,spos,svlos,sage,smet,gmass,gpos,ghsm,gmet,gsfr,lumtoflux = load_sim(snapfile,sim,objs,params,nproc,TINIT)
    Nobjs = len(iobjs)
# get original stellar mass at time of formation
    nstarsall = 0
    for i in range(Nobjs): 
        smass[i] = smass[i] / np.interp(np.log10(sage[i]*1.e9),fsps_ssp.ssp_ages,mass_remaining)  
        nstarsall += len(smass[i])

# compute extinction for all star particles
    #print('pyloser : Starting A_V calculation for nobj=%d ngas=%d nstar=%d [t=%g s]'%(Nobjs,len([i for il in gmass for i in il]),len([i for il in smass for i in il]),t_elapsed(TINIT)))
    print('pyloser : Starting A_V calculation for nobj=%d [t=%g s]'%(Nobjs,t_elapsed(TINIT)))
    kerntab = init_kerntab(params['kernel_type'])
    Solar = set_solar()
    lowZ_scaling = 'Simba_DTM'
    if eval(params['gas_metal_index'])==-1: lowZ_scaling = None  # Don't adjust dust content at low metallicity, since already using self-consistent dust mass which should account for this
    if nproc==1:
        A_V = [None]*Nobjs
        for i in range(Nobjs): 
            A_V[i] = compute_AV(int(params['view_dir']),gmass[i],gpos[i],ghsm[i],gmet[i],spos[i],Solar,sim.simulation.boxsize.value,sim.simulation.scale_factor,params['kernel_type'],kerntab,lowZ_scaling)
    else: 
        A_V = Parallel(n_jobs=nproc)(delayed(compute_AV)(int(params['view_dir']),gmass[i],gpos[i],ghsm[i],gmet[i],spos[i],Solar,sim.simulation.boxsize.value,sim.simulation.scale_factor,params['kernel_type'],kerntab,lowZ_scaling) for i in range(Nobjs))

# compute spectra and magnitudes in desired bands
    print('pyloser : Starting spectrum & magnitude calculation for %d objects/pixels with %d stars [t=%g s]'%(Nobjs,nstarsall,t_elapsed(TINIT)))
    # GALAXY PHOTOMETRY
    if params['output_type'] == 'phot':
        if nproc==1: 
            allmags = [None]*Nobjs
            for i in range(Nobjs): allmags[i] = calc_mags(smass[i],sage[i],smet[i],A_V[i],gsfr[i],Solar,lumtoflux,bands,ssp_interpolate,redshift,atten_curves)
        else: 
            allmags = Parallel(n_jobs=nproc)(delayed(calc_mags)(smass[i],sage[i],smet[i],A_V[i],gsfr[i],Solar,lumtoflux,bands,ssp_interpolate,redshift,atten_curves) for i in range(Nobjs))
    # INDIVIDUAL STELLAR MAGNITUDES
    elif 'star' in params['output_type']:
        sindex = np.zeros(nstarsall,dtype=np.int)  # set up object index for all stars
        oindex = np.zeros(nstarsall,dtype=np.int)  # set up count index for stars within objects
        ssfr = np.zeros(Nobjs)  
        nstars = 0
        for i in range(Nobjs):
            for j in range(len(smass[i])): 
                oindex[j+nstars] = i
                sindex[j+nstars] = j
            if sum(gsfr[i])>0: ssfr[i] = np.clip(np.log10(sum(gsfr[i])/sum(smass[i])+1.e-20)-9.,0.,1.)
            nstars += len(smass[i])
        if nproc==1: 
            allmags = [None]*nstars
            t0 = time.time()
            for i in range(nstars): 
                allmags[i] = calc_mags_star(smass[oindex[i]][sindex[i]],sage[oindex[i]][sindex[i]],smet[oindex[i]][sindex[i]],A_V[oindex[i]][sindex[i]],ssfr[oindex[i]],Solar,lumtoflux,bands,ssp_interpolate,redshift,atten_curves)
                progress_bar(1.*i/nstars,barLength=50,t=time.time()-t0)
        else: 
            allmags = Parallel(n_jobs=nproc,batch_size=(nstars//(4*nproc)+1))(delayed(calc_mags_star)(smass[oindex[i]][sindex[i]],sage[oindex[i]][sindex[i]],smet[oindex[i]][sindex[i]],A_V[oindex[i]][sindex[i]],ssfr[oindex[i]],Solar,lumtoflux,bands,ssp_interpolate,redshift,atten_curves) for i in range(nstars))
    # GALAXY SPECTRA AND PHOTOMETRY 
    elif params['output_type'] == 'spec':
        spec_npix = eval(params['spec_npix'])
        dlambda = 1.*(eval(params['spec_wave'])[1]-eval(params['spec_wave'])[0])/spec_npix
        wavelengths = np.arange(eval(params['spec_wave'])[0],eval(params['spec_wave'])[1],dlambda,dtype=float)  # set up desired wavelengths
        # compute LOS velocity = hubble flow from x[idir]=0 position plus LOS peculiar velocity
        idir = int(params['view_dir'])
        hubble = hubble_z(sim)
        vlos = [None]*Nobjs
        for iobj in range(Nobjs): vlos[iobj] = [spos[iobj][i][idir]*1.e-3*hubble + svlos[iobj][i] for i in range(len(spos[iobj]))]
        vlos = np.asarray(vlos)
        # compute magnitudes and spectra
        if nproc==1: 
            allmags = [None]*Nobjs
            t0 = time.time()
            nstarsdone = 0
            for i in range(Nobjs): 
                #if i%(max(Nobjs//20,1)) == 0: print('pyloser : doing galaxy %d nstar=%d [t=%g s]'%(i,len(smass[i]),t_elapsed(TINIT)))
                mags = calc_spec(smass[i],sage[i],smet[i],A_V[i],gsfr[i],Solar,lumtoflux,bands,wavelengths,vlos[i],eval(params['vsmooth_fwhm']),ssp_interpolate,redshift,atten_curves)
                allmags[i] = mags
                nstarsdone += len(smass[i])
                progress_bar(1.*nstarsdone/nstarsall,barLength=50,t=time.time()-t0)
        else: 
            allmags = Parallel(n_jobs=nproc,batch_size=(nstarsall//(32*nproc)+1))(delayed(calc_spec)(smass[i],sage[i],smet[i],A_V[i],gsfr[i],Solar,lumtoflux,bands,wavelengths,vlos[i],eval(params['vsmooth_fwhm']),ssp_interpolate,redshift,atten_curves) for i in range(Nobjs))
    else:
        print("I'm a loser baby: output_type",params['output_type'],"unrecognized.")
    print('pyloser : Done spectrum & magnitude calculation [t=%g s]'%t_elapsed(TINIT))

# parse and load magnitudes into appropriate arrays (and spectra if requested)
    allmags = np.asarray(allmags)
    if params['output_type'] == 'phot': allmags = np.reshape(allmags,(allmags.size//(4*len(bands)+2),4*len(bands)+2))[:Nobjs]
    if params['output_type'] == 'spec': allmags = np.reshape(allmags,(allmags.size//(4*len(bands)+2+2*spec_npix),4*len(bands)+2+2*spec_npix))[:Nobjs]
    mags = [i[0:len(bands)] for i in allmags]
    mags_nodust = [i[len(bands):2*len(bands)] for i in allmags]
    mags_app = [i[2*len(bands):3*len(bands)] for i in allmags]
    mags_app_nodust = [i[3*len(bands):4*len(bands)] for i in allmags]
    AV_galaxy = np.asarray([i[4*len(bands)] for i in allmags])
    L_FIR = np.asarray([i[4*len(bands)+1] for i in allmags])
    if params['output_type'] == 'spec': 
        spec = np.asarray([i[4*len(bands)+2:4*len(bands)+2+spec_npix] for i in allmags])
        spec_nodust = np.asarray([i[4*len(bands)+2+spec_npix:4*len(bands)+2+2*spec_npix] for i in allmags])
    else:
        spec = []
        spec_nodust = []
        wavelengths = []
    #print('pyloser : First 3 elements in mags array with shape ',np.shape(mags))
    #print('mags:',mags[:3])

    if params['map_type'] == 'image':         # decode pixel array into list of (x,y) indices
        Npix = eval(params['image_Npix'])
        iobjs = np.column_stack((iobjs%Npix[0],iobjs//Npix[0]))

    return mags,mags_nodust,mags_app,mags_app_nodust,A_V,AV_galaxy,L_FIR,iobjs,wavelengths,spec,spec_nodust


#=========================================================
# MAIN ROUTINE
#=========================================================

if __name__ == '__main__':

    # read in command line
    snapfile,caesarfile,parfile,outfile,nproc,params = parse_args(sys.argv)
    # Set up multiprocessing
    if nproc == 0: 
        try: nproc = eval(params['nproc'])
        except: nproc = 1
    if nproc != 1:
        import multiprocessing
        from joblib import Parallel, delayed
        from functools import partial
        num_cores = multiprocessing.cpu_count()
        if nproc < 0: print('Using %d cores (all but %d)'%(num_cores+nproc+1,-nproc-1) )
        if nproc > 1: print('Using %d of %d cores'%(nproc,num_cores))
    else: print('Using single core')

    # Read in caesar file
    if 'halo' in params['objs']: sim = caesar.load(caesarfile) 
    else: sim = caesar.load(caesarfile) 
    myobjs = eval(params['objs'])  # set the desired objects 
    Solar = set_solar()

    # Option 1: Do photometry or IFU for desired objects, in desired bands
    if params['output_type'] == 'phot' or params['output_type'] == 'spec' or 'star' in params['output_type']: 
        # Set up FSPS parameters
        print('pyloser : Using parameters')
        for k, v in params.items():
            print('\t %-30s %-20s'%(k, v))
        # set which metal to use for SSP -- see set_solar() for list
        star_metal_index = eval(params['star_metal_index'])    
        # create instance of SSP with provided parameters
        print('pyloser : Generating FSPS SSP [t=%g s]'%t_elapsed(TINIT))
        fsps_ssp = fsps.StellarPopulation(sfh=eval(params['fsps_sfh']),
                zcontinuous=eval(params['fsps_zcontinuous']),
                dust_type=eval(params['fsps_dust_type']),
                imf_type=eval(params['fsps_imf_type']),
                add_neb_emission=eval(params['fsps_add_neb_emission']))
        # Read in pre-existing SSP lookup table or generate new one to improve speed (if desired)
        try: ssp_lookup_file = params['ssp_lookup_table']
        except: ssp_lookup_file = ''
        if os.path.exists(ssp_lookup_file): 
            ssp_interpolate,mass_remaining = read_ssp_table(ssp_lookup_file,TINIT)
        else:
            ssp_interpolate,mass_remaining = generate_ssp_table(fsps_ssp,params,Solar[star_metal_index],ssp_lookup_file,TINIT)

        # Set up band info
        bands,bands_wavelength = init_bands(params['bands'],fsps_ssp.wavelengths)

        # Call main routine to compute magnitudes, etc
        mymags,mymags_nodust,mymags_app,mymags_app_nodust,AV_per_star,AV_galaxy,L_FIR,iobjs,wavelengths,spec,spec_nodust = pyloser(fsps_ssp,mass_remaining,ssp_interpolate,snapfile,sim,myobjs,nproc,bands)  

        # output hdf5 file
        with h5py.File(outfile, 'w') as hf:
            for key, value in params.items():
                hf.attrs.create(key, value, dtype=h5py.special_dtype(vlen=str)) # special_dtype required (see https://github.com/h5py/h5py/issues/289#issuecomment-329853651)
            hf.create_dataset('iobjs',data=iobjs)
            hf.create_dataset('mag_wavelengths',data=bands_wavelength)
            hf.create_dataset('absmag',data=mymags)
            hf.create_dataset('absmag_nodust',data=mymags_nodust)
            hf.create_dataset('appmag',data=mymags_app)
            hf.create_dataset('appmag_nodust',data=mymags_app_nodust)
            hf.create_dataset('AV_galaxy',data=AV_galaxy)
            hf.create_dataset('AV_per_star', data=AV_per_star)
            hf.create_dataset('L_FIR',data=L_FIR)
            if params['output_type'] == 'spec': 
                hf.create_dataset('spec_wavelengths',data=wavelengths)
                hf.create_dataset('spec',data=spec)
                hf.create_dataset('spec_nodust',data=spec_nodust)

    # Option 2: Do gas and other physical quantities
    elif params['output_type'] == 'gas' or params['output_type'] == 'datacube' or params['output_type'] == 'phys':
        # generate data cube for desired quantities
        iobjs,physmap,physcube = pygas(snapfile,sim,myobjs,params,nproc)
        # output hdf5 file
        quants = params['bands']
        print("pyloser : Outputting %s to hdf5 file %s [t=%g s]"%(quants,outfile,t_elapsed(TINIT)))
        with h5py.File(outfile, 'w') as hf:
            for key, value in params.items():
                hf.attrs.create(key, value, dtype=h5py.special_dtype(vlen=str))
            hf.create_dataset('iobjs',data=iobjs)
            hf.create_dataset('physmap',data=physmap)
            if params['output_type'] == 'datacube':
                for i in range(len(quants)):
                    hf.create_dataset(quants[i],data=physcube[i])
    else:
        print('pyloser : Output_type %s not recognized'%(params['output_type']))

    print('pyloser : Soy un perdedor, adios! [t=%g s]'%t_elapsed(TINIT))

