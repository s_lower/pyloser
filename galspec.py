

#=======================================================================
# Generates a color-mass diagram from pyloser hdf5 file (and caesar file)
#=======================================================================

import caesar
import pylab as plt
import numpy as np
import h5py
import sys
from load_pyloser import get_mags

script,caesarfile,loserfile = sys.argv # Note: igal must be a list e.g. [5] or [3,7] or [0-9]

if __name__ == '__main__':
    sim = caesar.load(caesarfile,LoadHalo=False) # load caesar file
# read spectra from pyloser file.
    params,spec = get_mags(loserfile,'spec')
    dlambda = 1.0*(eval(params['spec_wave'])[1]-eval(params['spec_wave'])[0])/eval(params['spec_npix'])
    mywave = np.arange(eval(params['spec_wave'])[0],eval(params['spec_wave'])[1],dlambda)

# get caesar info for these objects
    myobjs = eval(params['objs'])
    ms = np.asarray([i.masses['stellar'] for i in eval(params['objs'])])
    logms = np.log10(ms)
# plot stuff
    fig,ax = plt.subplots()
    plotgal = list(range(0,len(spec),100))
    print('plotting galaxies %s'%plotgal[:8])
    for igal in plotgal[:8]:
        if myobjs[igal].central != 1: continue
        specAA = spec[igal]/spec[igal][len(spec[igal])//2]        # normalize all spectra at some wavelength
        print(igal,specAA[:5])
        plt.plot(mywave,specAA,label=r'$M_*=%g$'%np.round(np.log10(ms[igal]),2))

    plt.minorticks_on()
    plt.ylabel(r'$F_\lambda$ (erg/s/cm$^2/\AA$)', fontsize=16)
    plt.xlabel(r'$\lambda (\AA)$' ,fontsize=16)
    if eval(params['fsps_add_neb_emission']) == True: note = 'with EL' # % np.round(sim.simulation.redshift,2)
    else: note = 'no EL' # % np.round(sim.simulation.redshift,2)
    plt.annotate(note, xy=(0.95, 0.05), xycoords='axes fraction',size=14,bbox=dict(boxstyle="round", fc="w"),horizontalalignment='right')
    plt.legend(loc='upper right')

    #plt.savefig('galspec.pdf', bbox_inches='tight', format='pdf')
    plt.show()


#######################################


